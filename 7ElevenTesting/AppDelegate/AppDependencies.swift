//
//  AppDependencies.swift
//  SevenEleven
//
//  Created by Atikom Tancharoen on 6/20/17.
//  Copyright © 2017 Atikom Tancharoen. All rights reserved.
//

import UIKit

class AppDependencies {
    
    func installRootViewController(withWindow window: UIWindow) {
        present(fromWindow: window)
    }
    
    private func present(fromWindow window: UIWindow) {
        // classes
        
        // setUp
        
        // present
    }
    
}
